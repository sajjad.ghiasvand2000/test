import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class GamePlay extends JPanel implements KeyListener , ActionListener {
//    private Rectangle rectangle = new Rectangle(500 ,2, 100, 478);
    private int birdCounter = 0;
    private int columnCounter = 0;
    private int columnCounter1 = 0;
    private int x;
    private int x1;
    private boolean play = false;
    private Timer timer;
    private int delay = 10;
    private int birdYPos = 250;
    private int culXPos = 800;
    private int culXPos1 = 800;
    private boolean isExist = false;
    private boolean isExist1 = false;

    public GamePlay(){
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(delay, this);
        timer.start();

    }

    public void paint(Graphics g){
        //bsky background border

        g.setColor(Color.yellow);
        g.drawRect(0, 0, 700, 600);
        //sky background
        g.setColor(Color.CYAN);
        g.fillRect(2, 2, 698, 480);
        //grass
        g.setColor(Color.green);
        g.fillRect(2 , 480 , 698 , 20);
        //grand
        g.setColor(Color.yellow);
        g.fillRect(2 , 500 , 698 , 100);

        //bird
        g.setColor(Color.RED);
        g.fillOval(350 , birdYPos , 15 , 15);

        //column

        if (isExist) {
            g.setColor(Color.PINK);
            g.fillRect(culXPos, 2, 100, x);
            g.fillRect(culXPos, 2 + x +120, 100, 478 -120 -x);
        }
        if (isExist1) {
            g.setColor(Color.PINK);
            g.fillRect(culXPos1, 2, 100, x1);
            g.fillRect(culXPos1, 2 + x1 +120, 100, 478 -120 -x1);
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        timer.start();

        if (play) {
            birdCounter++;
            columnCounter++;

            columnCounter1++;

            culXPos -= 3;
            culXPos1 -= 3;
            if (birdCounter == 50) {
                birdYPos += 40;
                birdCounter = 0;
            }
            if (columnCounter == 1){
                isExist = true;
                culXPos = 800;
                Random random = new Random();
                x = random.nextInt(350);

            }
            if (columnCounter == 400){
                isExist = false;
                columnCounter = 0;
            }

            if (columnCounter1 == 200){
                isExist1 = true;
                Random random = new Random();
                x1 = random.nextInt(350);
                culXPos1 = 800;

            }
            if (columnCounter1 == 600){
                isExist1 = false;
                columnCounter1 = 199;
            }
            Rectangle bird = new Rectangle(350 , birdYPos , 15 , 15);
            Rectangle rec11 = new Rectangle(culXPos, 2, 100, x);
            Rectangle rec12 = new Rectangle(culXPos, 2 + x +120, 100, 478 -120 -x);
            Rectangle rec21 = new Rectangle(culXPos1, 2, 100, x1);
            Rectangle rec22 = new Rectangle(culXPos1, 2 + x1 +120, 100, 478 -120 -x1);
            if (bird.intersects(rec11) || bird.intersects(rec12) || bird.intersects(rec21) || bird.intersects(rec22)){
                play = false;

            }
        }
        repaint();
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
            play = true;
            birdYPos -= 50;
        }
    }


    @Override
    public void keyTyped(KeyEvent keyEvent) {}
    @Override
    public void keyReleased(KeyEvent keyEvent) {}

    public void draw(Rectangle rectangle){

    }
}
