import javax.swing.*;

public class MainFrame extends JFrame {

    private GamePlay gamePlay;

    public MainFrame(){
        setSize(700, 600);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Flappy bird");
        gamePlay = new GamePlay();
        add(gamePlay);
    }
}
